use std::sync::mpsc::{Receiver, Sender};
use std::thread;

use image::{ImageBuffer, Luma};

use super::foundry::Grid;

use super::bkmessages::{BackendCommand, BackendUpdate};

const image_buffer_path: &str = "/tmp/automata_buffer.png";

pub fn launch(cmd_chan: Receiver<BackendCommand>, update_chan: Sender<BackendUpdate>) {
    thread::spawn(move || {
        let mut grid: Option<Grid> = None;
        let width = 300;
        let height = 200;

        loop {
            match cmd_chan.recv() {
                Ok(cmd) => {
                    match cmd {
                        BackendCommand::Randomize => {
                            if grid.is_none() {
                                grid = Some(Grid::new_random(
                                    &String::from("#Toroidal Life"),
                                    true,
                                    &vec![2, 3],
                                    &vec![3],
                                    width,
                                    height,
                                ));
                            } else {
                                grid.as_mut().unwrap().randomize();
                            }
                        }
                        BackendCommand::NextGen(n) => {
                            if grid.is_none() {
                                update_chan
                                    .send(BackendUpdate::Err(String::from("grid not initialized")))
                                    .unwrap();

                                continue;
                            } else {
                                for _ in 0..n {
                                    grid.as_mut().unwrap().next_gen();
                                }
                            }
                        }
                        BackendCommand::Clear => {
                            grid = Some(Grid::new(
                                &String::from("#Toroidal Life"),
                                true,
                                &vec![2, 3],
                                &vec![3],
                                width,
                                height,
                            ))
                        }
                    }

                    let width = grid.as_ref().unwrap().get_width();
                    let height = grid.as_ref().unwrap().get_height();
                    let mut bytes: Vec<u8> = Vec::with_capacity(width * height);

                    for y in 0..height {
                        for x in 0..width {
                            bytes.push(grid.as_ref().unwrap().get_cell_state(x as i64, y as i64));
                        }
                    }

                    let img =
                        ImageBuffer::<Luma<u8>, _>::from_raw(width as u32, height as u32, bytes)
                            .unwrap();
                    img.save(image_buffer_path).unwrap();

                    update_chan
                        .send(BackendUpdate::NewState(String::from(image_buffer_path)))
                        .unwrap();
                }
                Err(_) => {
                    println!("Backend error, exiting!");
                    break;
                }
            }
        }
    });
}
