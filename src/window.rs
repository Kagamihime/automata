use gdk_pixbuf::Pixbuf;
use gtk::prelude::*;

use std::sync::mpsc::{Receiver, Sender};
use std::sync::{Arc, RwLock};

use bkmessages::{BackendCommand, BackendUpdate};

pub fn new_window(
    cmd_chan: Arc<RwLock<Sender<BackendCommand>>>,
    update_chan: Arc<RwLock<Receiver<BackendUpdate>>>,
) -> gtk::ApplicationWindow {
    let builder: gtk::Builder =
        gtk::Builder::new_from_resource("/org/gnome/Automata/gtk/window.ui");

    let window: gtk::ApplicationWindow = builder
        .get_object("app_window")
        .expect("Application window could not be loaded");

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        gtk::Inhibit(false)
    });

    let grid_da: gtk::Image = builder.get_object("grid_da").expect("grid_da not found");
    let btn_rand: gtk::Button = builder
        .get_object("randomize_button")
        .expect("randomize_button not found");
    btn_rand.connect_clicked(clone!(cmd_chan, update_chan => move |_| {
            cmd_chan.read().unwrap().send(BackendCommand::Randomize).unwrap();

            let mut path = String::new();

            match update_chan.read().unwrap().recv().unwrap() {
                BackendUpdate::NewState(p) => path = p,
                BackendUpdate::Err(msg) => println!("Error: {}", msg),
            }

            grid_da.set_from_file(path);
    }));

    let grid_da: gtk::Image = builder.get_object("grid_da").expect("grid_da not found");
    let btn_ng: gtk::Button = builder
        .get_object("next_gen_button")
        .expect("next_gen_button not found");
    btn_ng.connect_clicked(clone!(cmd_chan, update_chan => move |_| {
        cmd_chan.read().unwrap().send(BackendCommand::NextGen(1)).unwrap();

        let mut path = String::new();

        match update_chan.read().unwrap().recv().unwrap() {
            BackendUpdate::NewState(p) => path = p,
            BackendUpdate::Err(msg) => println!("Error: {}", msg),
        }

        grid_da.set_from_file(path);
    }));

    let grid_da: gtk::Image = builder.get_object("grid_da").expect("grid_da not found");
    let btn_clear: gtk::Button = builder
        .get_object("clear_button")
        .expect("clear_button not found");
        btn_clear.connect_clicked(clone!(cmd_chan, update_chan => move |_| {
            cmd_chan.read().unwrap().send(BackendCommand::Clear).unwrap();

            let mut path = String::new();

            match update_chan.read().unwrap().recv().unwrap() {
                BackendUpdate::NewState(p) => path = p,
                BackendUpdate::Err(msg) => println!("Error: {}", msg),
            }

            grid_da.set_from_file(path);
        }));

    let mnu_about: gtk::MenuItem = builder
        .get_object("about_menu")
        .expect("about_menu not found");

    {
        let window = window.clone();
        mnu_about.connect_activate(move |_| {
            show_about(&window);
        });
    }

    return window;
}

fn show_about(window: &gtk::ApplicationWindow) {
    let builder: gtk::Builder = gtk::Builder::new_from_resource("/org/gnome/Automata/gtk/about.ui");

    let about_dialog: gtk::AboutDialog = builder
        .get_object("about_dialog")
        .expect("about_dialog not found");
    about_dialog.set_modal(true);
    about_dialog.set_transient_for(window);
    about_dialog.show();
}
