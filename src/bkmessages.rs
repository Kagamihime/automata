pub enum BackendCommand {
    Clear,
    Randomize,
    NextGen(u32),
}

pub enum BackendUpdate {
    NewState(String),
    Err(String),
}
