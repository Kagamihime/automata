use gio::prelude::*;
use gtk;
use gtk::prelude::*;

use std::sync::mpsc::channel;
use std::sync::{Arc, RwLock};

use crate::backend;
use crate::window;
use bkmessages::{BackendCommand, BackendUpdate};

pub fn launch() {
    let app: gtk::Application =
        gtk::Application::new("org.gnome.Automata", gio::ApplicationFlags::empty())
            .expect("Could not create application");

    let (cmd_sender, cmd_receiver) = channel::<BackendCommand>();
    let cmd_sender = Arc::new(RwLock::new(cmd_sender));

    let (update_sender, update_receiver) = channel::<BackendUpdate>();
    let update_receiver = Arc::new(RwLock::new(update_receiver));

    backend::launch(cmd_receiver, update_sender);

    app.connect_startup(move |app| {
        let w: gtk::ApplicationWindow =
            window::new_window(cmd_sender.clone(), update_receiver.clone());
        w.set_application(app);
        w.show_all();
    });

    app.run(&std::env::args().collect::<Vec<_>>());
}
