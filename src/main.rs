extern crate gdk_pixbuf;
extern crate gio;
extern crate glib;
extern crate gtk;

extern crate image;

extern crate foundry;

// http://gtk-rs.org/tuto/closures
#[macro_export]
macro_rules! clone {
    (@param _) => ( _ );
    (@param $x:ident) => ( $x );
    ($($n:ident),+ => move || $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move || $body
        }
    );
    ($($n:ident),+ => move |$($p:tt),+| $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move |$(clone!(@param $p),)+| $body
        }
    );
}

mod app;
mod backend;
mod bkmessages;
mod static_resource;
mod window;

fn main() {
    gtk::init().expect("Error initializing gtk.");
    static_resource::init().expect("Something went wrong with the resource file initialization.");

    app::launch();
}
